module.exports = {
  title: 'Mantis@Uppsala',
  description: 'Mantis blog @Uppsala',
  keywords: ['Bönsyrsor', 'bönsyrsa', 'uppsala'],
  url: 'https://mantis.uppsala.ai', // your site url without trailing slash
  paginate: 6 // how many posts you want to show for each page
  // uncomment the next line if you want to add disqus to your site
  // disqusShortname: "your-shortname"
};
