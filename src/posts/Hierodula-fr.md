---
title: "Une mante pour débutant"
date: 2024-10-26T14:00
thumb: "baby_bw.jpg"
tags:
    - Mantis
---

Article FR/EN
## Pourquoi Hierodula ?

L' espèce Hierodula venosa est très facile et ne nécessite pas d'hygrométrie particulière, elles survivent bien dans mon appart à 19+ degré sans ajout d'aucun autre substrat qu'un sopalin, se laissent oublier une semaine en cas de vacances, et un bon terra peut se faire avec de la récup (grand pot de cornichon, un filet + elastique, quelques branches de ci de là et c'est parti.). Le seul investissement que je recommande est un spray ou un brumisateur pour leur donner de l'eau qui ne soit pas contaminée par des parfums ou produits chimiques. Il faut aussi des insectes pour leur nourriture, (je peux fournir un début de culture de drosophiles pour commencer), sinon laisser  pourrir une banane dans votre salon marche très bien et fera du plus bel effet pour vos invités :D. Et quand elles seront plus grandes, il faudra trouver des proies en fonction de leur taille, et de ce que vous avez sous la main (les blattes de votre vide ordure, les asticots de pêche de votre papy, la tipule qui est rentrée dans votre salle de bain...)

## Why Hierodula ?
The species Hierodula venosa is very easy and does not require any particular hygrometry, they survive well in my apartment at 19+ degrees without adding any other substrate than a paper towel, can be forgotten for a week in case of vacation, and a good terrarium can be made with recycling (large pickle pot, a net + elastic band, a few branches here and there and off you go.). The only investment I recommend is a spray or a mist sprayer to give them water that is not contaminated by perfumes or chemicals. You also need insects for their food, (I can provide a start of a culture of drosophila to start), otherwise leaving a banana to rot in your living room works very well and will look great for your guests :D. And when they are bigger, you will have to find prey depending on their size, and what you have on hand (the cockroaches in your garbage disposal, the fishing maggots from your grandpa, the crane fly that got into your bathroom, etc.)

### Photo by @james_j.z

![Photo of the father](/assets/img/baby_bw.jpg "Hierodula venosa stade L3 photo par Jin Zhao instagram: @james_j.z")

---

