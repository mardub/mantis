---
title: "Mealworms experiment / Mjölmaskar experiment"
date: 2024-01-30T14:00
thumb: "durum_flours.jpg"
tags:
    - Mealworms
---

## A little experiment...

... about mealworms! 50 days ago I wanted to experiment to figure out if some old dry food found in my drawers could be safely used for my mealworms farm.
For all the experiments the temperature was around 18C. There was two bowls of water next to the experiments to maintain a bit of humidity but one of them dried out which may have induce more canibalism.
1. In 4,5 ml of graham flour + 3 ml of sesame seeds
Before: 5 worms + 4 miniworm
After: 2 beatles, 1 pupate, 1 big worm, 1 medium size worm, 1 miniworm
Survival rate: 6/9, maybe 8/9 as two miniworms missing. 66 to 88%
Note: One extra pupate is there but eaten so all the big worms have survived until pupating. 3 miniworms are missing, my eyes being tired they may be hidden in the flour.

2. 7,5 ml of crushed rie kernels / rågkross
Before: 4 big worms + 4 miniworms
After: 2 miniworms, 1 big worm, one beatle
Survival rate: 50% 4/8
Note: The remains of a eaten pupate has been found, my conclusion is that the worms have done some canibalism maybe the substrate was too dry?

3. 7,5 ml of weat berries
Before: 5 mealworms, 4 miniworms
After: 2 pupates, 3 bigworms, 1 medium size worm and 3 miniworms
Survival rate: 80% to 100% 8/9
Note: The miniworms are still very small. One of the pupates is black, probably dead but not eaten by its mates. I put 80 to 100% as the pupate is potentially dead but was not eaten it means the substrate was not too dry.

Conclusion: Do not use rie kernels. The rest should be good enough



![Rie kernels](/assets/img/rie_kernels.jpg "8 worms left in 7,5 ml of rie kernels for 50 days")

## Mjölmask experiment
Jag har gjort en 50 dagar mjölmask experiment. Vetekärnor är bäst substrate, rågkross är sämsta.

---

