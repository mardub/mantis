---
title: "Premier envoi!"
date: 2024-10-28T14:00
thumb: "first_pack1.jpg"
tags:
    - Mantis
---

Article ES/EN
## Hola Alejandro

¡Espero que tu ooteca de Hierodula venosa te llegue sana y salva! Aquí tienes algunos datos que te pueden resultar útiles.

### Trazabilidad y fechas importantes
- Fecha de puesta: 6 de octubre de 2024. Es la tercera ooteca que me regala la hembra, se apareó con el macho a principios de julio de 2024.
- La madre fue comprada en la etapa L2/L3 a [mantibulle.fr](https://www.mantibulle.fr) en enero de 2024.
- El padre fue comprado en la etapa L6 a [tropichome.fr](https://www.tropichome.fr) el 4 de abril de 2024 en la feria francesa de insectos [Le peuple de l'ombre](https://www.facebook.com/associationlepeupledelombre)

### Cuidados
Para tu información, la primera ooteca eclosionó después de 40 días durante el verano y las altas temperaturas. El segundo no ha eclosionado todavía después de 57 días, por lo que no esperaría ninguna eclosión antes de la primera semana de diciembre.


* actualización 8 de noviembre: La segunda ooteca eclosionó el 1 de noviembre, es decir, 61 días de incubación. Por lo tanto, podemos estimar la eclosión de su nidada alrededor del 6 de diciembre.
Esta segunda nidada dio alrededor de 130 mantis, pero nuevamente fue más grande que la nidada que recibió. *

La primera ooteca dio a luz a unos 140 individuos. La ooteca que tienes es más pequeña y, por lo tanto, es probable que dé menos. No fue necesario ningún cuidado especial para la ooteca, excepto fijarla en un recipiente grande y ventilado y rociarla con agua cada tres días.

Finalmente, la mejor manera de agradecer, si tienes tiempo, es avisarme si eclosionaron sin problemas y cuándo. Me encanta mantener datos sobre las mantis y cómo se desarrollaron.

¡Buena suerte!

### Foto del padre (a la izquierda)

![Photo of the father](/assets/img/male_venosa.jpg "On the left the male Hierodula venosa, on the right a Hierodula doveri sp")

### Photo de la madre
![Photo of the father](/assets/img/female_venosa.jpg "The mom.")

### Photo of packaging
This will be sneaked into a large enveloppe with extra straw and soft material to limit chock.

![Photo of the packaging](/assets/img/first_pack2.jpg "A camembert box wrapped in a plastic box with wholes.")

## Hi Alejandro

Hope your ootheca of Hierodula venosa arrives safely to you! Here are some data that you might find helpful.


### Traceability and important dates
- Date it was layed: 6th october 2024. It is the third ootheca given by my female she was mated with the male begining of july 2024.
- The mother has been bought at stage L2/L3 from [mantibulle.fr](https://www.mantibulle.fr) in January 2024. 
- The father was bought at stage L6 from [tropichome.fr](https://www.tropichome.fr) the 4th April 2024 on French insect fair [Le peuple de l'ombre](https://www.facebook.com/associationlepeupledelombre)

### Care
For information the first ootheca hatched after 40 days during summer and hot temperatures. The second has not hatched yet after 57 days. So I would not expect any hatchs before the first week of december.

 The first ootheca gave birth to ~140 individuals. The ootheca you have is smaller and thus will provably give less. No particular care was needed for ootheca except from fixing it up to a large ventilated container and spraying water every three day.  
 
 Finally the best way to thanks if you have time is to let me know if they hatched safely and when! I love keeping data about the mantis and how they developped.

Good luck!

### Photo of the father (on the left)
![Photo of the father](/assets/img/male_venosa.jpg "On the left the male Hierodula venosa, on the right a Hierodula doveri sp")
---

