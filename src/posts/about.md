---
title: "About mantis.uppsala.ai"
date: 2023-10-20T19:00
thumb: "Edvard_Munch_Vampire.jpg"
tags:
    - other
    - EN
    - SV
---

# What ?

mantis.uppsala.ai is 100% google free, static (thus [saves energy saving](https://solar.lowtechmagazine.com/) and ecofriendly!), and open source: you can download its template in the following gitlab: [gitlab.com/mardub/mantis](https://gitlab.com/mardub/mantis).

# Who ?

The main maintainer of this blog is Marie D. both living in Lille and Uppsala and rediscovering an old passion for mantids. She is also helped by Tilo W. for open source consultation, support and as well as for caring for mantis and other bugs while she is away. Olivia Å. is also a contributer by ensuring mantid reproduction, and care. And some of the most wonderful macro photos are made by [Jin Zhao](https://www.instagram.com/james_j.z/) please do not copy his photos without permission.

Note that this blog is not sponsored and is made on our free time.

# Why ?

L'élevage de mante n'est pas mon activité principale. Les conseils disposés ici ne sont que nos observations personnelle et ne sont peut-être pas extensible à des élevages pros. A titre personel (Marie) je me donne pour objectif avec ce blog de : mener une réflexion éthique autour de l'élevage dans notre contexte mondialisé de libre échange, créer un espace pérenne de retour d'expérience en dehors des GAFAM et autres plateformes ayant un impact social et environemental négatif. Je doute que Zuzu et Musk aient cure de la survie des insectes alors mon travail intellectuel ne servira pas leur business. En libriste invétérée je profiterai de ce blog pour encourager les gens dans un internet plus vertueux, je ne vous encourage pas ni à me suivre, ni à vous abonner à quoi que ce soit. Un email fera bien plaisir et surtout vous rencontrer lors d'échange et de bourse.

Kanske ska jag skriva på svenska men det är inte min modersmål då bli snäll!

<!-- ![random image](/assets/img/image.jpg) -->

# Contact

mantis [@] uppsala.ai 
