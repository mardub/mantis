---
title: "Deuxième naissance, dons à Oignies"
date: 2024-11-02T14:00
thumb: "default.jpg"
tags:
    - Mantis
    - FR
    - Echange
    - Hierodula venosa
---

Article FR
## Chers Ludo, [Marie](https://www.facebook.com/profile.php?id=61556704501276), [Terrazoo](https://www.terrazoo.fr), [Mantibulle](https://mantibulle.fr/], Millenac,

Merci pour vos dons échanges et autres services rendus en échange des mantes. A titre indicatif je vous laisse toutes les informations que j'ai à propos des mantes que vous avez recu de ma part à la bourse de Oignie.
## Tracabilité
- Espèce: Hierodula venosa.
- La mère a été achetée au stade L3/4 en janvier 2024 sur le site de [mantibulle.fr](https://www.mantibulle.fr)
- Le père a été acheté au stade Subadulte à [tropichome.fr](https://www.tropichome.fr) le 4 avril 2024 en la bourse [Le peuple de l'ombre](https://www.facebook.com/associationlepeupledelombre)

## Dates importantes
Vos mantes sont toutes nées entre le 1er et le 3 Novembre 2024 d'une deuxième ponte après une incubation à température ambiante de 61 jours. La femelle a pu donner 3 pontes jusque là.

Certaines boites n'avaient qu'un compte aproximatif si vous avez pu les compter et avez le temps je serais contente de savoir combien vous en avez recu au final pour garder la trace du nombre de naissance par oothèque.

Au plaisir de vous revoir au détour des bourses.

PS: si pour une raison ou pour une autre vous ne souhaitez pas être cité ici merci de me le signifier par email, aucune justification n'est nécessaire je respecterai votre anonymat.

M.
### Note to self
Combien de petits au total.
Ludo -> ~60
Millénac -> 11
Terrazoo -> 13
Pets invazing -> 15+
Mantibulle -> Benco 20 ?? + 3
Reste: 7
Mort: 1 
Total naissance ponte 2 = 130 (estimation)
