---
title: "Fiche Hierodula venosa"
date: 2024-11-01T14:00
thumb: "juliet.jpg"
tags:
    - Mantis
---

Article ES/EN
# Fiche technique

# Hierodula venosa (Olivier, 1792)


## Informations générales

- Origine : Asie du Sud-Est
- Taille adulte : environ 10 cm
- Durée de vie : 1 an
- Alimentation : Insecte taille adaptée. Eviter les grillons, fourmies interdites.

## Soins et Comportement

- Température idéale : entre 20 et 25 degrés Celsius, avec une humidité modérée.

- Alimentation :
    Insectivore, elles se nourrissent de tout ce qui bouge et est plus petit qu'elles. On évitera cependant les grillons d'élevage (vecteurs de maladie), et les fourmies.

- Comportement :
    Ces insectes sont bien adaptés pour le camouflage ; ils se fondent parmi les feuilles. Voraces ils n'hésiteront pas à s'attaquer à toute proie légèrement plus petites qu'eux. On veillera à les séparer pour éviter le cannibalisme.

- Reproduction :
    Les femelles et les males s'accouplent à l'âge adulte (c'est à dire lorsqu'ils/elles ont des ailes). Elles pondent un sac d'oeufs nommé oothèque qui peut donner naissances à plus d'une centaine de petits.

- Habitat :
    Préparez un terrarium avec un substrat adapté, et assurez-vous qu'il y ait suffisamment de branches ou grilles pour que votre mante puisse grimper. Les mantes muent grâce à la gravitation en se laissant pendre par les pattes. On recommande donc un terrarium haut d'au moins 3 fois la longueur de l'animal avec beaucoup d'endroit où se supendre. Vaporisez légèrement tous les deux à trois jours pour donner à boire et maintenir un peu d'humidité. Gardez le terrarium bien aéré pour éviter toute moisissure: une moustiquaire comme couvercle est parfait.
    
- Mue : Les mantes passent par plusieurs stades de mue au cours de leur croissance. Lors de chaque mue, elles se débarrassent de leur ancienne cuticule pour en révéler une nouvelle, plus grande. Cette mue ne prend que quelques minutes, 30 tout au plus, mais 24h avant la mue la mante cessera de se nourrir. Il est donc important de laisser l'animal en paix et de retirer les proies qui pourraient la déranger pendant ce procédé. Si elle perd une patte pas d'inquiétude: elle repoussera à la prochaine mue (si elle n'est pas adulte!).

- Manipulation :
    Elles sont calmes et peuvent être manipulés délicatement, mais restent fragiles. Soyez donc prudent.

### Photo d'une Hierodula venosa femelle jeune (L4)

![Photo Juliet](/assets/img/juliet.jpg "Hierodula venosa L4")


Bonne Chance!

---

