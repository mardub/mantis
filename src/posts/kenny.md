---
title: "Kenny"
date: 2024-12-16T15:00
thumb: "kenny.jpeg"
tags:
    - mantis
    - EN
---

---

# Kenny

Article EN

Dear Dalia, Kailin, Dann, Sush...

Thank you for maintaining Kenny alive during christmas and January. I am away from Wednesday 18 dec, and will be back in Febaruary, probably before.

I prepared for you a "Kenny kit" in it you will find:

* a box of drosophiles ( don't worry they cannot fly!)
* 1 box of mealworms
* A feather
* Tweezers
* '(in the fridge) some pupate of big flies. Keep in fridge otherwise they will all hatch!
* An extra plastic box (for catching flies or if you want to carry Kenny safely home).
* Toothpicks

Ideal rythm of feeding depends on size of prey. He is now big enough to eat 2 drosophiles a day, one or two big flies a week.

Kenny is pretty resilient, if you must leave him for a long time (like a week or more) drop at least 3 pupate of big flies in his enclosure so that some hatch in a couple of days. As usual do not hesitate to spray him water whenever you come in to work.

If the drosophile box start to stink feel free to close it with the cap that has wholes and if it is too gross and stinky just throw them away, there should be enough mealworms and big flies to feed him.

## Tricks:

* Mantids like sweets, a bit of honey can be given on top of a toothphics and I left a drop on top of his cage for extra nutrition over christmas leave.
* Mealworms are good back up but can be tricky to give, you can use tweezers to hand it to him, let one snick from top of cage when he is hanging ou there... Do not leave them at the bottom, Kenny will not see them.
* Feel free to manipulate and have Kenny run on your hands, he cannot bite. Feather may help pushing him away if you are really scared of hurting him. Otherwise fingers, he is more resistent than you think.
* If he refuses to eat after several attempt do not insist and check regularly the cage for the upcoming show: he might be molting in the next 24 hours. Avoid moving him or his cage while molt happen.

If any worries, question or comment call me any time. And if Kenny dies don't worry, it is life! his 100 other brothers and sisters will come as replacement :).