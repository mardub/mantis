---
title: "Stockholm-Expo"
date: 2024-09-27T14:00
thumb: "female_venosa.jpg"
tags:
    - Mantis
---

## Hi

Congratulations for getting the Hierodula venosa. I am Marie amateur insect keeper and I thought these information could be useful.

### Traceability and important dates
- Date of birth: Thursday 5th September 2024
- Date of last molt: They had their first molt around the 20th of september 2024 they are L2.
- The mother has been bought at stage L2/L3 from [mantibulle.fr](https://www.mantibulle.fr) in January 2024. 
- The father was bought at stage L6 from [tropichome.fr](https://www.tropichome.fr) the 4th April 2024 on French insektmässa [Le peuple de l'ombre](https://www.facebook.com/associationlepeupledelombre)

### Care

Hierodula venosa lives well in room temperature and are easy mantis species. If you have raised up mantis before they will be no difficulty for you. If not, do not hesitate to ask questions here +46 (null) 7 20 40 33 13 I am passionate about it!

Photos of the father and mother can be found on my website: [https://mantis.uppsala.ai/](https://mantis.uppsala.ai/)

Good luck and have fun breeding them!

Marie.

### Photo of the father

![Photo of the father](/assets/img/male_venosa.jpg "On the left the male Hierodula venosa, on the right a Hierodula doveri sp")
---

