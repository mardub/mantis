---
title: "Terrarium"
date: 2024-11-01T14:00
thumb: "bocal.jpg"
tags:
    - Mantis
---

Article FR
## Le terrarium du pau.. de l'écolo.

Je voudrais ici donner des inspirations de terrariums possibles pour votre mante. Si vous restez dans les espèces faciles qui ne requièrent pas une humidité "amazonienne" vous n'avez pas besoin d'acheter des terrariums neuf. Soyez juste un peu créatif et surtout obsevateur. Et collectez tout récipient transparent qui vous semblerait pratique, et testez ce qui semble convenir.

# 1. Le vase ou bocal
Ce "terrarium" en photo n'est autre qu'un vase ouvert sur le dessous auquel j'ai ajouté une vieille assiette à dessert pour socle. Le vase a été trouvé en seconde main. L'avantage est qu'en pouvant soulever par le dessous je pouvais nourir, et nettoyer sans déranger. Tous les branchages sont récupérés dans les arbustes et parcs de la ville. Le subsrat un simple sopalin permet d'être changé et d'observer plus facilement les restes, mues et  et déchets laissés par la mante.
![Vase](/assets/img/bocal.jpg "my first terrarium")
Attention le verre est très glissant pour les mantes, on veillera à mettre le plus de choses agripantes possibles (branches bien rugueuses, grillage, filet...).
## Une verrine recyclée
Cette verrine a été percée d'un trou pour ajouter une ouverture en moustiquaire, elle est utilisée pour l'exposition le transport des toutes petites mantes.
![Verrine](/assets/img/diamant.jpg "Verrine")
![Verrine](/assets/img/diamant2.jpg "Verrine")
# 2. Couvercles
Fermez votre terrarium à l'aide de quelque chose *qui laisse passer l'air*, cela peut-être beaucoup de trous, un filet fixé avec de la colle ou du file à un pourtour en plastique, bois ou cahouchou adapté à votre récipient. Un tissus et un elastique marche aussi mais c'est moins pratique.

# 3. La boite à fromage.
Mes mantes adorent ces boites grillagées! Elles s'y accrochent partout, et m'accompagnent en voyage, la boite peut pivoter dans tous les sens la mante ne bouge pas. Je l'utilise surtout pour les mantes agées. J'en ai même eu une qui est spontanément rentré dans la boite après être grimpé au rideau.
![Boite à fromage](/assets/img/fromage.jpg "Boite à fromage")
![Boite à fromage](/assets/img/fromage2.jpg "Boite à fromage")

# 4. Rien (pas pour les anxieux)

Cela va paraître étonnant mais si vous vous sentez à l'aise vous pouvez essayer de laisser la mante en liberté. Je dirais même qu'il vaut mieux une bonne plante sans terra qu'un mauvais terrarium. Une fois la mante assez grande pour être visible aisément posez là sur votre plante de salon. Observez si elle trouve un spot confortable. 

Va-t-elle disparaître ? Oui. Si elle disparaît ne paniquez pas! Après vous être assuré vite fait qu'elle n'est pas tombée au sol (très rare) regardez vers le haut. Les mantes grimpent, elles adorent les rideaux et les plafonds. Regardez les coins où elle peut se percher. Et si elle n'est pas là attendez quelques heures. Elles réapparaissent généralement au bout de 48h.

Avertissement: bien sûr ce n'est pas sans risque: utilisez votre bon sens, ne faites pas cela si vous avez enfants, animaux de compagnie, évitez les pots de fleurs à cache-pots avec beaucoup d'eau où elles peuvent se noyer. Ne laissez pas les fenêtre grand ouvertes sans surveillance, ne les mettez pas sur le balcon ni jardin. Et si votre mante est agitée et s'amuse à disparaître plusieurs fois de suite remettez là en terra. Les males en chaleurs peuvent parfois être trop agité pour cela.

# Conclusion

Il n'y a pas un mais des terrarium ideal en fonction de la taille de votre mante et de ce que vous avez à disposition. Le seul dénominateur commun est: de quoi s'accrocher/grimper et toujours plus de 3 fois la longueur de la mante pour laisser place à la mue, et bien aérer. Aprés tout est affaire d'observation: regardez comment la mante évolue essayez différents types de branchages et boites et voyez si elle semble à l'aise.

---
