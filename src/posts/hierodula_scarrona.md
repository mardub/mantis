---
title: "Hierodula Scarrona"
date: 2025-02-24T19:00
thumb: "measure1.jpeg"
tags:
    - mantis
    - EN
---

---

# Hierodula scarrona

Article EN

This article still in draft state, and will get update as we get more info. Check date of publication above. Anyone can contribute and bring some correction via this link: <https://cloud.fripost.org/s/zSD2zLBEzFLxGsx> let me know if so, so that I push the updates on website. Note that no AI has been used for writing this article.

## Abstract:

This article reports the discovery of an unidentified female praying mantis that may belong to a non-documented species. The specimen was discovered with leg damaged in 2024 during dry season in Tamil Nadu at subadult stage and was kept in captivity until natural death occurs. Dead specimen was sent to Thomas Röchen for further investigation. All the photos of the individual made during its lifetime, including photos of measurements on this article are downloadable fullsize as a zipfile [here](https://gitlab.com/mardub/mantis/-/archive/main/mantis-main.zip?path=src/posts/img/scarron).

## Introduction

## Discovery

On 18 march 2024  a subadult of a mantid of type Hierodulini has been discovered in the Thanjavur district (India). The geo-localisation is: [10°48'55.3"N 79°14'54.4"E](https://www.openstreetmap.org/?mlat=10.83&mlon=79.23#map=5/10.83/79.23), discovery occured at 9AM, during dry season.  The altitude is between 0 and 300 meters, and the temperature at this season varies around 35 degrees. The biotope is a cultivated forest of coco trees, papaya trees, banana trees. A photo of this biotope at 5 meters from her discovery place can be seen below.

<img src="/assets/img/IMG_20240318_092232_1.jpg" alt="Photo of biotope" style="max-width: 20%; height: auto;">

The subadult specimen was found in distress on the ground with posteral left metathoracic leg  missing (only coxa and trochater remained) and metathoracic right leg cut at the end of the tibia [¹]. The legs have entirely regrown during maginal molting though the left one previously missing is functional but atrophied.

<img src="/assets/img/discovery.jpg" alt="Photo at discovery time" style="max-width: 20%; height: auto;">

## Measurements and description

Measurements have been performed at adult stage. To avoid ambiguity they are presented in photos below with arrows showing from where to where is taken each measurement. In the photos and this article we try as much as possible to respect the nomenclature as describred in the publication of  [ ^ Brannoch, 2017]

<img src="/assets/img/measure1.jpeg" alt="First measurements" style="max-width: 50%; height: auto;">

<img src="/assets/img/Antenna_cercus.JPG" alt="Second  measurements antennas" style="max-width: 50%; height: auto;">

<img src="/assets/img/measure3.JPG" alt="Third  measurements" style="max-width: 50%; height: auto;">

<img src="/assets/img/Tibia.JPG" alt="Tibia" style="max-width: 50%; height: auto;">

<img src="/assets/img/spines.JPG" alt="spines" style="max-width: 50%; height: auto;">

<img src="/assets/img/wings.JPG" alt="Wings" style="max-width: 50%; height: auto;">

We have a lot of photos of her in the file ziped [here](https://gitlab.com/mardub/mantis/-/archive/main/mantis-main.zip?path=src/posts/img/scarron) . Among them let's show a sample of main charateristical parts.

<img src="/assets/img/raptor.jpg" alt="Raptor" style="max-width: 20%; height: auto;">

<img src="/assets/img/genitals.jpg" alt="genitals" style="max-width: 20%; height: auto;">

## Discussions on identification

Three observations about her have been posted with the website inaturalist.org a centralised website to promote citizen-science where amateurs and scientists can discuss specimens observed in nature.

- The first at subadult stage: <https://www.inaturalist.org/observations/207564807>

- Second at adult stage: <https://www.inaturalist.org/observations/207564809>

- Third at adult stage with measurements: <https://www.inaturalist.org/observations/209114611>

We encourage the reader to consult the observations and the debates, sometimes animated, that this specimen raised. The exercise of identifying Hierodulas over just photo is fastidious, some hypothesis have been mentioned such as, Titanodula grandis, Hierodula doveri but none of these proposition made a consensus. In this paper we refer to our specimen as a "Hierodula" because of its green color and its provenance from asia but it is not impossible that it belongs to another genus, above all given the revisions of nomenclature that occured in the last years [Vermeersch. 2020]. This article is named "Hierodula scarrona" because  we found her with two legs missing: as a shortcut we refered to her daily as "Scarron" as the famous french author that had disabled legs.

I myself possessed living specimen of Hierodula membranacea and Hierodula venosa of the same age and could observe both of them together alive. Comparatively they were significantly different, even for a non-trained entomological eye.

Below the photo of our specimen (on the right) compared to a male Hierodula venosa (on the left)

<img src="/assets/img/comparision.jpg" alt="Comparison scarrona venosa" style="max-width: 20%; height: auto;">

The specimen present at first sight all the attribute of Hierodula that we compared it to, green color, wings... With closer look we observe some distinguishable features:

* Although the size is not always a reliable key, it is to be noted that she is relatively small, similar to Hierodula patellifera: 65 mm.
* there is presence of white to beige spines on the coxa of the raptors. However these are too small and too numerus (5) to be Hierodula patellifera
* Presence of beige and brown small dot on the forewing.
* Her green color is very "basil green" unlike for instance the Hierodula venosa that had a more bluish green. And when compared to a live specimen of a male Hierodula membranacea her legs were much more uniformly green when the membranacea presented pinkish-red gradients.

The comparative analysis is to be continued as we need the comparisons to be made on other species by someone with large collections, and as for now I have met only 3 sorts commonly sold in europe: membranacea, venosa, patellifera.

## Captivity

### Conditions, date of molt, dates of oothecas

The specimen has been kept at room temperature i.e, around 20 ( up to 27 degrees during summer time from June to August). In these conditions it molted to adult on 13 Avril i.e,. 26 days after capture.

She layed 7 oothecas. The first was layed on the 4 of June (i.e., 39 days after adulthood). The second was layed on 21st of July, (i.e., 47 days after first lay). The third ootheca was layed around week 34. The 4th ootheca was layed on exactly the 1st of september. The 5th ootheca was layed on 25th of september. Between this date and the date of her death 2 oothecas got layed but discovered too late to approximate a date.

The first ootheca was layed on the branch of an orchid branch at 1,70 m from the ground. The second ootheca was layed on a curtain at only 30 cm from the ground. The five other  ootheca were layed in her enclosure on wood borders, mostly because the 4 last months she spent more and more time in her enclosure for practical reasons. The 5 oothecas found in enclosure were sent to T. Rönisch for further expertise.

### Feeding

It was fed on a variety of insects, default feeder being Calliphora vomitoria at fly and larvae stage. On average we tried giving one per day. On occasion we fed it multiple other live prays and types of food including: mealworms (Tenebrio molitor) nymph and adult, common european bees, common european wasp, morio worms, butterfly...

At adult stage we noticed that she was still able to notice and chase drosophiles of medium size (i.e 2 mm long.) despite her relatively big size.

Finally on rare occasion we tried very small quantities of non-insect food: mango, banana (raw and flambé), orange, fresh raspberries from local garden, apple, honey, egg, lamb, fish. The only fruit not tolerated was lemon.

## 

## Observations and behavior

### Life

Most of her life we kept her free roaming in a room of 40 sqm. Most of the time she stayed around a window to hide in plants and curtains. We observed her disappearing on 3 occasion during the interval between maginal molt and first oothecas where she was particularly agitated. Toward the 3 last months of her life we kept her more and more in the enclosure, and exclusively in her enclosure the three last weeks for two reasons: 1) on one occasion we observed her returning spontaneously to her open enclosure despite us placing her far outside on plants. 2) At the very end she started to be weaker, we wanted to maintain her hunting behaviour and avoid feeding with twizzers thus a limited enclosure was more appropriate.

The enclosure was a 25 by 20 cm french cheese box which has the same properties as a rigid flexarium:

<img src="/assets/img/fromage.jpg" alt="Photo of her enclosure" style="max-width: 50%; height: auto;">

Couple of weeks after maginal molt we observed mating behavior such as curl of the abdomen toward ground. During a night we have surprised her standing less than 30 cm from a male Hierodula venosa curling down abdomen and bending head down. To avoid any accident we seperated the specimens thus we have only observed this behavior once. The male did not react to these advances. Inspired by [Kelner-Pillault S (1957)] she was kept on an enclosure that had an open grid with both a free roaming male membranacea and male venosa, none of the adult gentlemen tried aproching her[^2]. Her calling behavior got significantly reduced after laying of the second ootheca.

<img src="/assets/img/calling.jpg" alt="Calling behavior" style="max-width: 30%; height: auto;">

### Death

The specimen had a long yet normal decay. The atrophied leg started  darkening from the tip in early August 2024 loosing use of the tarsus. As she could not reach this leg for cleaning we suspect that it got some infection. A dark spot appeared below one of her right mesothoracic leg below proximal bend in the tibia, which she could still move but seem to avoid using. The two last days we had her it was hard to establish if she was dead as she was absolutely lethargic during the day, not reacting and at some hours of the evening could suddenly give sign of reactions or a spasm on one leg. We are unsure when the death occured and rigidity remained to be confirmed by the receiver.

<img src="/assets/img/goodby.jpg" alt="Package for last voyage to Germany" style="max-width: 40%; height: auto;">

## Summary of data

* Date of discovery: 2024-03-18
* Gender: Female
* Stage: Subadult, adult (from 2024-04-13)
* Date of Death: 2024-12-10?
* Place: Edavakkudi, Tamil Nadu, India
* Size: 65 mm
* Number of Oothecas: 7
* Color: Green
* Link to original full size 200+ photos: <https://gitlab.com/mardub/mantis/-/archive/main/mantis-main.zip?path=src/posts/img/scarron>


## Conclusion

We reported the discovery, life, and debates of a yet to be identified specimen of perhaps Hierodula. Although damaged on legs it survived long after collect. Since no one has yet come up with an identification, we temporarily name the specimen "Hierodula scarrona" after the French author Scarron who also had damaged posterior legs. The specimen is awaiting for further analysis by Thomas Rönchen. This article is the opportunity to experiment collaboration between academically trained entomologists and hobby mantis breeders. As claimed by [Moulin, 2020] and [Battiston et. al 2022] these type of collaboration are the opportunity to extend our knowledge of the species.

### Acknowledgement

 We want to thanks Raazesch Saudin proprietary of the farm Vakeworks in which this mantis was found. Thanks to your organic cultivation exempt from artificial pest control, specimens like Scarron can survive. And on this matter we want to thanks Kavitha Murugesan from Keela Kottur Papanasam Taluk in Thanjavur District. Kavitha is the local farmer doing everyday care of Scarron's forest with sutainable technics, it is thanks to the hard work of people like her that this mantis can continue to exist and we think the occidental scientific community needs to acknowledge that. If any serious publication was to come, I hope to call this species after your name if it is ok with you. We want also to thanks all the users of inaturalist.org, in particular [arjun_mantises](https://www.inaturalist.org/people/arjun_mantises "arjun_mantises") for his incredible good will and the contacts he gave for me to get further contacts with enthomologist. Thank you also to Thomas Rönisch who accepted to collect her for identification, please keep both scientist and hobbyist community informed ! Thank you to Tilo Wiklund for taking the time to give Scarron water with her favorite water pipe while I was on vacation even when she was stubbornly hooked on the ceiling.

[^1]: At discovery what remained of the right leg was being bitten by a large ant holding on her, we don't know if this predator was responsible for her state. The molt came only a month later thus the amputation may also have been caused by molting accident.

[^2]: At least we never found any male approaching her cage neither daytime neither night time. Several factor could lead to this results, including individual variations. The male Hierodula venosa had hard time approaching a female of his own species as well. Although this observation of free roaming males in presence of the female cage were led for only a two to three weeks period. Thus not as rigourusly as in the publication of [Kelner-Pillault S (1957)] Both males were at the time adult but relatively young (less than 2 month after maginal molt) though they were pretty agitated which could indicate that they were both ready for reproduction. 

## Reference

Brannoch SK, Wieland F, Rivera J, Klass K-D, Béthoux O, Svenson GJ (2017) Manual of praying mantis morphology, nomenclature, and practices (Insecta, Mantodea). ZooKeys 696: 1-100. <https://doi.org/10.3897/zookeys.696.12542>

Battiston R, Di Pietro W, Anderson K (2022) ﻿The pet mantis market: a first overview on the praying mantis international trade (Insecta, Mantodea). Journal of Orthoptera Research 31(1): 63-68. <https://doi.org/10.3897/jor.31.71458>

Xavier H.C. Vermeersch. 2020. *Titanodula* gen. nov., A New Genus of Giant Oriental Praying Mantises (Mantodea: Mantidae: Hierodulinae). ***Belgian Journal of Entomology.*** *100*: 1–18.

Moulin N (2020) When Citizen Science highlights alien invasive species in France, the case of Indochina mantis, *Hierodula patellifera* (Insecta, Mantodea). Biodiversity Data Journal 8: e46989. <https://doi.org/10.3897/BDJ.8.e46989>

Kelner-Pillault S (1957) Attirance sexuelle chez *Mantis religiosa*. Bulletin de la Société entomologique de France 62: 9–11.

---

<script>  
document.addEventListener("DOMContentLoaded", () => {  
document.querySelectorAll('img[alt]').forEach(img => {  
if (!img.title) {  
img.title = img.alt;  
}  
});  
});  
</script>
